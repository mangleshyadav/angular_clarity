import { Component, OnInit } from '@angular/core';
import {Comparator} from "clarity-angular";
import {
  Student,
  StudentService
} from './student.service';

@Component({
  templateUrl: './student-list.component.html'
})
export class StudentListComponent implements OnInit {
  studentes: Promise<Student[]>;
  msg = 'Loading contacts ...';
  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.displayMessage(this.msg);
    this.studentes =  this.studentService.getStudentes();
  }
  displayMessage(msg: string) {
    this.msg = msg;
    setTimeout(() => this.msg = '', 1000);
  }
}
