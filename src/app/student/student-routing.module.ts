import { NgModule }     from '@angular/core';
import { Routes,
         RouterModule } from '@angular/router';

import { StudentComponent }       from './student.component';
import { StudentListComponent }   from './student-list.component';
import { StudentDetailComponent } from './student-detail.component';

const routes: Routes = [
  { path: '',
    component: StudentComponent,
    children: [
      { path: '',    component: StudentListComponent },
      { path: ':id', component: StudentDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule {}
