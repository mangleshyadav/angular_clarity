"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var student_service_1 = require('./student.service');
var StudentDetailComponent = (function () {
    function StudentDetailComponent(route, studentService) {
        this.route = route;
        this.studentService = studentService;
    }
    StudentDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = parseInt(this.route.snapshot.params['id'], 10);
        this.studentService.getStudent(id).then(function (student) { return _this.student = student; });
    };
    StudentDetailComponent = __decorate([
        core_1.Component({
            template: "\n\n  <div class=\"card\">\n   <div class=\"card-header\">\n    <small highlight>Student Detail</small>\n    </div>\n    <div class=\"card-block\">\n    <div *ngIf=\"student\">\n      <div>Id: {{student.id}}</div><br>\n      <label>Name:\n        <input [(ngModel)]=\"student.name\">\n      </label>\n    </div>\n    <br>\n    <a routerLink=\"../\">Student List</a>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, (typeof (_a = typeof student_service_1.StudentService !== 'undefined' && student_service_1.StudentService) === 'function' && _a) || Object])
    ], StudentDetailComponent);
    return StudentDetailComponent;
    var _a;
}());
exports.StudentDetailComponent = StudentDetailComponent;
//# sourceMappingURL=hero-detail.component.js.map