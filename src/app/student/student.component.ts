// Exact copy except import UserService from core
import { Component }   from '@angular/core';

import { StudentService } from './student.service';
import { UserService } from '../core/user.service';

@Component({
  template: `
 
    <router-outlet></router-outlet>
  `,
  providers: [ StudentService ]
})
export class StudentComponent {
  userName = '';
  constructor(userService: UserService) {
    this.userName = userService.userName;
  }
}
